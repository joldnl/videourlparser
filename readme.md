# Video Url Parser
Transform a url of a supported video service into an embed on your site.
This class provides you with an easy way of embedding video's into your project, just by using the url of the video page. Most major video providers are supported.

Supported Services:
 - Youtube
 - Vimeo
 - Facebook

### Features:
 - Supports Youtube, Vimeo and Facebook
 - Transform vimeo, youtube and facebook video links into embedded video's
 - Easily get embed urls from video links
 - Get video embed markup from a video link
 - Get video key from a video url
 - Get embed code to use on your site


## Installation:
Load the class:

```php
<?php require_once('VideoUrlParser.class.php'); ?>
```

## Usage example:
```php
<?php
// Get the class file
require_once('VideoUrlParser.class.php');

// Instanciate the class as a variable
$VideoUrlParser = new VideoUrlParser;

// Have some test urls ready
$youtube        = 'https://www.youtube.com/watch?v=ozAPGnr-934&t=33/asd';
$vimeo          = 'https://vimeo.com/77131852?foo=barr&lala=blabla';
$facebook       = 'https://www.facebook.com/antonio.navarro.79025648/videos/1705569846133711?permPage=1';

// Set the video service url
// Change this to $vimeo or $facebook to test those services
$video_url      = $youtube;

// Get the diferent details from the video url
$get_service    = $VideoUrlParser->get_service( $video_url );       // Get the video provider
$get_key        = $VideoUrlParser->get_key( $video_url );           // Get the video key or ID
$get_embed_url  = $VideoUrlParser->get_embed_url( $video_url );     // Get the video embed url
$get_embed_code = $VideoUrlParser->get_embed_code( $video_url );    // Get the video Embed html code

// Show the embedded video:
echo $get_embed_code;

```


The output is different per video service, but the default output is this:

**Youtube and Vimeo:**
```html
<iframe src="{{video-url}}" width="640" height="360" frameborder="0" allowfullscreen></iframe>
```

**Facebook:**
```html
<iframe src="https://www.facebook.com/plugins/video.php?href={{video-url}}&show_text=false" width="640" height="360" frameborder="0" allowFullScreen></iframe>
```

## Customize video output:
Check what get_service returns (either one of the services), and write your own custom markup around that, by using the ```get_embed_url``` method.

## Methods

#### get_service( $video_url );
Return the name of the video provider from the provided $video_url.
Returns either ```'youtube'```, ```'vimeo'``` or ```'facebook'```.

#### get_key( $video_url );
Get the video id from $video_url (the youtube, vimeo or facebook link).
Returns the unique video id from the provided url.

#### get_embed_url( $video_url );
Get the embed url from $video_url (the youtube, vimeo or facebook link).

#### get_embed_code( $video_url );
Get the video embed code (html markup) name from $video_url (the youtube, vimeo or facebook link).


## Usage

By default the class outputs pre-formatted html markup for you, nut if you need custom wrappers, you're completely free to do so.
In stead of using the the ```get_embed_code``` method, just write your own markup and use the ```get_embed_url``` method to get the correct video embed url.
