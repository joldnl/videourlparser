<?php

/**
 *
 * Video Url Parser
 *
 * Parses URLs from major cloud video providers. Capable of returning
 * keys from various video embed and link urls to manipulate and
 * access videos in various ways.
 *
 * @author       Jurgen Oldenburg <info@jold.nl>
 * @version      v1.0.1
 *
 * @TODO:        Get facebook video url by video-id trough the facebook graph API
 * @TODO:        Add more video providers like twitter, instagram, dumpert and liveleak
 *
 */


class VideoUrlParser {



    /**
     *
     * @var $_services
     * The supported video services
     *
     * @access  public
     * @since   1.0.0
     *
     */
    public $_services;



    /**
     *
     * __construct.
     *
     * Main class costructor function
     *
     * @type       function
     * @date       2017/09/19
     * @author     Jurgen Oldenburg
     * @since      1.0.0
     *
     * @param      array    $services     The list of services
     *
     * @return     n/a
     *
     */
    public function __construct() {

        // Set the supported services and their details,
        // if they are not yet set
        if( empty( $this->_services ) ) {
            $this->_services = $this->services();
        }

    }



    /**
     *
     * services
     *
     * A list of supported video services and their properties
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      n/a
     * @return     array     $services     The list of supported services
     *
     */
    private function services() {

        if ( ! $this->_services ) {

            $services = [
                'youtube'  => [
                    'key'       => 'youtube',
                    'match'     => '%youtube|youtu\.be%i',
                    'base_url'  => 'https://youtube.com/embed/',
                    'template'  => '<iframe src="%url%?ecver=2" width="640" height="360" frameborder="0" allowfullscreen></iframe>',
                ],
                'vimeo'    => [
                    'key'       => 'vimeo',
                    'match'     => '%vimeo%i',
                    'base_url'  => 'https://player.vimeo.com/video/',
                    'template'  => '<iframe src="%url%" width="640" height="360" frameborder="0" allowfullscreen></iframe>',
                ],
                'facebook' => [
                    'key'       => 'facebook',
                    'match'     => '%facebook|fb\.me%i',
                    'base_url'  => 'https://www.facebook.com/',
                    'template'  => '<iframe src="https://www.facebook.com/plugins/video.php?href=%url%&show_text=false" width="640" height="360" frameborder="0" allowFullScreen></iframe>',
                ],
                'twitter' => [
                    'key'       => 'twitter',
                    'match'     => '%twitter.com%i',
                    'base_url'  => 'https://twitter.com/',
                    'template'  => '<blockquote class="twitter-video" data-lang="en"><a href="%url%"></a></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>',
                ]
            ];

            $this->_services = $services;

        }

        return $this->_services;

    }



    /**
     *
     * get_video_service
     *
     * Determines which cloud video provider is being used based on the passed url.
     *
     * @type       function
     * @access     public
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string          $url    The url
     * @return     null|string             False on failure to match, the service's name on success
     *
     */
    public function get_service( $url ) {

        // Load the supported services
        $services = $this->_services;

        // Check if $url is set
        if ( isset($url) && !empty($url) ) {

            // Check if $services is set and an array
            if ( !empty($services) && is_array($services) ) {

                // Compare supported services against the $url
                foreach ( $services as $service ) {

                    // Return the video service key if it exists as suported service
                    if ( preg_match( $service['match'], $url ) ) {
                        return $service['key'];
                    }

                }

            }

        }

        return false;

    }



    /**
     *
     * get_key
     *
     * Determines which cloud video provider is being used based on the passed url,
     * and extracts the video id from the url.
     *
     * @type       function
     * @access     public
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string          $url    The url
     * @return     string|boolean          False on failure to match, the video's id on success
     *
     */
    public function get_key( $url ) {

        $service = $this->get_service( $url );

        // Check if the url is a valid supported service
        if ( $service ) {

            $function = 'get_key_' . $service;
            return $this->$function( $url );

        }

        return false;

    }



    /**
     *
     * get_youtube_key
     *
     * Get the video key from a Youtube url
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string      $url    The Youtube url
     * @return     string              The key of the Youtube video
     *
     */
    private function get_key_youtube( $url ) {

        $url_keys = [ 'v', 'vi' ];

        // Try to get ID from url parameters
        $key_from_params = $this->util_parse_params( $url, $url_keys );

        if ( $key_from_params ) {
            return $key_from_params;
        }

        // Try to get ID from last portion of url
        return $this->util_parse_last_element( $url );

    }



    /**
     *
     * get_key_vimeo
     *
     * Get the video key from a Vimeo url
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string          $url    The Vimeo url
     * @return     boolean|string          The key of the Vimeo video
     *
     */
    private function get_key_vimeo( $url ) {

        $parsed_key = $this->util_parse_last_element($url);

        // Try to get ID from last portion of url
        if ( ! empty( $parsed_key ) ) {

            return $parsed_key;

        }

        return false;

    }



    /**
     *
     * get_key_facebook
     *
     * Get the video key from a Facebook video url
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string              $raw_url    The Facebook video url
     * @return     boolean|string                  The key of the Facebook video
     *
     */
    private function get_key_facebook( $raw_url ) {

        $url        = parse_url( $raw_url );
        $path       = explode( "/", $this->util_trim( $url['path'] ) );

        // Prepare the path details array
        $details    = [
            'host'  => $url['scheme'] . '://' . $url['host'],
            'path'  => $path,
            'fb'    => [
                'user'  => $path[0],
                'type'  => $path[1],
                'key'   => $path[2]
            ]
        ];

        $key = $this->util_trim( $details['fb']['user'] . '/' . $details['fb']['type'] . '/' . $details['fb']['key'] );

        // Check if the fb url is a video url.
        if ( $details['fb']['type'] == 'videos' ) {
            return $key;
        }

        return false;

    }



    /**
     *
     * get_key_twitter
     *
     * Get the video key from a Twitter status video url
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string              $raw_url    The Twitter video url
     * @return     boolean|string                  The key of the Twitter video
     *
     */
    private function get_key_twitter( $raw_url ) {

        $url        = parse_url( $raw_url );
        $path       = explode( "/", $this->util_trim( $url['path'] ) );

        // Prepare the path details array
        $details    = [
            'host'  => $url['scheme'] . '://' . $url['host'],
            'path'  => $path,
        ];

        $key = $this->util_trim( $details['path'][0] . '/' . $details['path'][1] . '/' . $details['path'][2] );

        // Check if the Twitter url is a video url.
        if ( $details['path'][1] == 'status' ) {
            return $key;
        }

        return false;

    }




    /**
     *
     * get_embed_url
     *
     * Get the formatted video url, dependant of the video provider
     *
     * @param      string          $url    The input video page url
     * @return     boolean|string          The formatted embed url
     *
     */
    public function get_embed_url( $url = '' ) {

        if ( !empty($url) ) {

            $service    = $this->get_service( $url );
            $key_fn     = 'get_key_' . $service;
            $video_key  = $this->$key_fn( $url );

            // Check if the url is a valid supported service
            if ( $service ) {

                $services = $this->_services;

                return $services[$service]['base_url'] . $video_key;

            }

        }

        return false;

    }



    /**
     *
     * get_embed_code
     *
     * Return the formatted html video embed code
     *
     * @param      string          $url    The input video page url
     * @return     boolean|string          Return formatted html embed code as setup in the services settings.
     *
     */
    public function get_embed_code( $url ) {

        if ( $url ) {

            $service    = $this->get_service( $url );
            $key_fn     = 'get_key_' . $service;
            $video_key  = $this->$key_fn( $url );

            // Check if the url is a valid supported service
            if ( $service ) {

                $services = $this->_services;
                $template = $services[$service]['template'];

                if ( $service == 'facebook' ) {
                    $compiled = str_replace( '%url%', urlencode($services[$service]['base_url'] . $video_key), $template );
                } else {
                    $compiled = str_replace( '%url%', $services[$service]['base_url'] . $video_key, $template );
                }

                return $compiled;

            }
        }

        return false;
    }



    /**
     *
     * util_trim
     *
     * Remove all spaces and leading/taling (left and right) slash from a string
     *
     * @param      string          $string    The input string to check
     * @return     boolean|string  The string without leading or tailing slash.
     *
     */
    private function util_trim( $string = '' ) {

        // Check if the input is not empty
        if ( !empty($string) ) {

            // Strip leading and tailing slashes
            $output = $this->util_ltrim( $string );
            $output = $this->util_rtrim( $output );

            return $output;
        }

        return false;

    }



    /**
     *
     * util_ltrim
     * Remove spaces and leading (left) slash from a string
     *
     * @param      string          $string    The input string to check
     * @return     boolean|string  The string without leading slash.
     *
     */
    private function util_ltrim( $string = '' ) {

        // Check if the input is not empty
        if ( !empty($string) ) {
            return ltrim( $string, '/' );
        }

        return false;

    }



    /**
     *
     * util_rtrim
     * Remove spaces and tailing (right) slash from a string
     *
     * @param      string          $string    The input string to check
     * @return     boolean|string  The string without tailing slash.
     *
     */
    private function util_rtrim( $string = '' ) {

        // Check if the input is not empty
        if ( !empty($string) ) {
            return rtrim( $string, '/' );
        }

        return false;

    }



    /**
     *
     * util_parse_params
     *
     * Find the first matching parameter value in a url from the passed params array.
     *
     * @access     private
     * @type       function
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string           $url             The url
     * @param      array            $target_params   Any parameter keys that may contain the id
     *
     * @return     null|string                       Null on failure to match a target param, the url's id on success
     *
     */
    private function util_parse_params( $url, $target_params ) {

        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_params );

        foreach ( $target_params as $target ) {
            if ( array_key_exists( $target, $my_array_of_params ) ) {
                return $my_array_of_params[$target];
            }
        }

        return null;

    }



    /**
     *
     * util_parse_last_element
     *
     * Find the last element in a url, without any trailing parameters
     *
     * @type       function
     * @access     private
     * @date       2017/09/19
     * @since      1.0.0
     *
     * @param      string              $url        The url
     * @return     string                          The last element of the url
     */
    private function util_parse_last_element( $url ) {

        $url_parts    = explode( "/", $this->util_ltrim( $url ) );
        $prospect     = end( $url_parts );

        // Check if the last item of the path is a param
        $prospect_params = preg_split( "/(\?|\=|\&)/", $prospect );

        if ( $prospect_params ) {
            return $prospect_params[0];
        } else {
            return $prospect;
        }

        // Bail out and return original url
        return $url;

    }




}
