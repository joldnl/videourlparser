<?php

    // Get the class file
    require_once('VideoUrlParser.class.php');

    // Instanciate the class as a variable
    $VideoUrlParser = new VideoUrlParser;

    // Have some test urls ready
    $youtube        = 'https://www.youtube.com/watch?v=ozAPGnr-934&t=33/asd';
    $vimeo          = 'https://vimeo.com/77131852?foo=barr&lala=blabla';
    $facebook       = 'https://www.facebook.com/antonio.navarro.79025648/videos/1705569846133711?permPage=1';
    $twitter        = 'https://twitter.com/RadioFarda_Eng/status/910418253905788928';


    // Set the used video url
    $video_url      = $twitter;

    // Get the diferent details from the video url
    $get_service    = $VideoUrlParser->get_service( $video_url );
    $get_key        = $VideoUrlParser->get_key( $video_url );
    $get_embed_url  = $VideoUrlParser->get_embed_url( $video_url );
    $get_embed_code = $VideoUrlParser->get_embed_code( $video_url );

    // Debug the video details
    // var_dump( $get_service );
    // var_dump( $get_key );
    // var_dump( $get_embed_url );
    // var_dump( $get_embed_code );

?>


<html>
    <body>
        <h1>Service: <?php echo $get_service; ?></h1>
        <section>

            <?php echo $get_embed_code; ?>

        </section>
    </body>
</html>
